package ltos.call.recorder.automatic.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitClient  {
    private val BASE_URL = "http://punchapp.in/api/"
    private val BASE_URL_AD = "https://stage-ads.punchapp.in/api/"

    fun getFeedbackClient(): RetrofitInterface
         {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(RetrofitInterface::class.java)
        }

    fun getAdClient(): RetrofitInterface
         {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL_AD)
                .build()
            return retrofit.create(RetrofitInterface::class.java)
        }
}