package ltos.call.recorder.automatic.activity

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import androidx.appcompat.app.AppCompatActivity
import ltos.call.recorder.automatic.adapter.SelectedContactAdapter
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.utility.MyContact
import ltos.call.recorder.automatic.databinding.ActivitySelectedContactBinding
import org.koin.android.ext.android.inject


class SelectedContactActivity : AppCompatActivity() {

    private val myPref: MySharedPrefernce by inject()
    private var binding: ActivitySelectedContactBinding? = null
    private var selectedContactAdapter: SelectedContactAdapter? = null
    private var from = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectedContactBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        
        from = intent?.extras?.getString("for").toString()


        setTitleText()
        setupRecyclerView()
        setAllClick()
    }

    private fun setTitleText() {
        when(from)
        {
            "contactRecord" -> {
                binding?.textTitle?.text = "Contact to Record"
            }
            "contactIgnore" -> {
                binding?.textTitle?.text = "Contact to Ignore"
            }
        }
    }

    private fun setupRecyclerView() {
        selectedContactAdapter = SelectedContactAdapter(from)
        binding?.rcvContact?.apply {
            adapter = selectedContactAdapter
        }
    }

    private fun setAllClick() {
        binding?.floatingButton?.setOnClickListener {
            val contactPickerIntent = Intent(
                Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI
            )
            startActivityForResult(contactPickerIntent, 1)
        }

        binding?.backbtn?.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == RESULT_OK)
        {
            when(requestCode)
            {
                1 -> {
                    val cursor = data?.data?.let { contentResolver.query(it, null, null, null, null) }
                    cursor?.moveToFirst()
                    val phoneNumberIndex = cursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    val displaNameIndex = cursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                    var phoneNumber = phoneNumberIndex?.let { cursor.getString(it) }
                    val displayName = displaNameIndex?.let { cursor.getString(it) }

                    when(phoneNumber?.startsWith("+"))
                    {
                       true->{
                           phoneNumber = phoneNumber.substring(3).trim()

                       }
                    }
                    phoneNumber =phoneNumber?.replace(" ","")

                    val myContact = phoneNumber?.let { displayName?.let { it1 -> MyContact(it1, it) } }
                    myContact?.let { selectedContactAdapter?.contactList?.add(it) }

                    selectedContactAdapter?.contactList?.size?.let {
                        selectedContactAdapter?.notifyItemInserted(
                            it
                        )
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        selectedContactAdapter?.contactList?.let {
            myPref.setContactList(
                from,it
            )
        }
    }


}