package ltos.call.recorder.automatic.utility

data class MyContact(val displayName: String, val phoneNumber: String)
