package ltos.call.recorder.automatic.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaRecorder
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.activity.MainActivity
import ltos.call.recorder.automatic.receiver.CallStatusReceiver
import ltos.call.recorder.automatic.singleton.FileUtility
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.singleton.TempData
import org.koin.android.ext.android.inject
import java.io.IOException


class RecorderService : Service() {
    private var CHANNEL_ID: String? = "CallRecordingService"

    private var audioManager: AudioManager? = null
    private val myPref: MySharedPrefernce by inject()
    private val fileutility: FileUtility by inject()
  /*  private var audioRecord:AudioRecord? = null
    private var isRecordingAudio = false
    private var recordingState:Thread? = null

    private val RECORDER_SAMPLE_RATE = 44100
    private val AUDIO_SOURCE = MediaRecorder.AudioSource.VOICE_RECOGNITION
    private val CHANNEL_CONFIG = AudioFormat.CHANNEL_IN_MONO
    private val AUDIO_FORMAT = AudioFormat.ENCODING_PCM_8BIT
    private val BUFFER_SIZE = AudioRecord.getMinBufferSize(RECORDER_SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT)*/



    /* private var audioRecord:AudioRecord? = null
     private var isRecordingAudio = false
     private var recordingState:Thread? = null*/

    override fun onCreate() {
        super.onCreate()

        createNotificationChannel()
        setupNotification()
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        if (mediaRecorder == null) {
            startMediaRecorder()

        }
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    fun startMediaRecorder() {

        if (mediaRecorder != null) {
            mediaRecorder = null
        }
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager?.setParameters("noise_suppression=auto")

        mediaRecorder = MediaRecorder()
        mediaRecorder?.setAudioEncodingBitRate(128000)
        mediaRecorder?.setAudioSamplingRate(44100)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            audioManager?.setStreamVolume(
                AudioManager.STREAM_VOICE_CALL,
                audioManager!!.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL),
                0
            )
            mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION)
        } else {
            mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL)
        }

        when (myPref.getString("AudioFormat")) {
            "amr" -> {
                mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.AMR_WB)
                mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB)
            }
            "3gp" -> {
                mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC_ELD)
            }
            "aac" -> {
                mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS)
                mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            }
            "mp4" -> {
                mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mediaRecorder?.setOutputFile(fileutility.outPutFile)
        } else {
            mediaRecorder?.setOutputFile(fileutility.outPutFile.path)
        }
        try {
            mediaRecorder?.prepare()
            mediaRecorder?.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun stopMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder?.stop()
            mediaRecorder?.release()
            mediaRecorder = null
            CHANNEL_ID = null
            TempData.callnumber = ""
            CallStatusReceiver.serviceStarted = 1
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Call Recorder",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }

    fun setupNotification() {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID!!)
            .setContentTitle("Call Reocrder")
            .setSmallIcon(R.drawable.notification)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)
    }

    override fun onDestroy() {
        stopMediaRecorder()
        super.onDestroy()
    }

    companion object {
        var mediaRecorder: MediaRecorder? = null
    }

    /*private fun startAudioRecorder() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        audioRecord = AudioRecord(AUDIO_SOURCE, RECORDER_SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT, BUFFER_SIZE);

        if(audioRecord?.getState() != AudioRecord.STATE_INITIALIZED)
        {
            return;
        }
        else
        {
            isRecordingAudio = true;
        }
        audioRecord?.startRecording()

        recordingState = Thread( {
            writeAudioDataToFile()
        })
        recordingState?.start()
    }

    private fun stopAudioRecorder() {
        if (audioRecord != null) {
            isRecordingAudio = false
            audioRecord?.stop()
            audioRecord?.release()
            audioRecord = null
            recordingState = null
            TempData.callnumber = ""
            CallStatusReceiver.serviceStarted = 1
        }
    }

    private fun writeAudioDataToFile() {
        val data = ByteArray(BUFFER_SIZE)
        val mRecordFile = fileutility.outPutFile
        var fos: FileOutputStream?
        try {
            fos = FileOutputStream(mRecordFile)
        } catch (e: FileNotFoundException) {

            fos = null
        }
        if (null != fos) {
            var chunksCount = 0
            val shortBuffer = ByteBuffer.allocate(2)
            shortBuffer.order(ByteOrder.LITTLE_ENDIAN)
            //TODO: Disable loop while pause.
            while (isRecordingAudio) {
                    chunksCount += audioRecord?.read(data, 0, BUFFER_SIZE)!!
                    if (AudioRecord.ERROR_INVALID_OPERATION != chunksCount) {
                        var sum: Long = 0
                        var i = 0
                        val shorts = ShortArray(shortBuffer.array().size / 2)
                        while (i < BUFFER_SIZE) {

                            //TODO: find a better way to covert bytes into shorts.
                            shortBuffer.put(data[i])
                            shortBuffer.put(data[i + 1])
                            sum += Math.abs(shortBuffer.getShort(0).toInt()).toLong()

// to turn bytes to shorts as either big endian or little endian.
// to turn bytes to shorts as either big endian or little endian.
                            ByteBuffer.wrap(shortBuffer.array()).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer()[shorts]
                            shortBuffer.clear()
                            i += 2
                        }
                        try {
                            if(isAudible(shorts))
                            fos.write(data)
                        } catch (e: IOException) {

                        }
                    }

            }
            try {
                fos.close()
            } catch (e: IOException) {

            }
            setWaveFileHeader(mRecordFile, 1)
        }
    }

    private fun isAudible(data: ShortArray): Boolean {
        val rms = getRootMeanSquared(data)
        Log.e("TAG", "isAudible: $rms")
        return rms > 196 &&  rms<5000
    }

    private fun getRootMeanSquared(data: ShortArray): Double {
        var ms = 0.0
        for (i in data.indices) {
            ms += (data[i] * data[i]).toDouble()
        }
        ms /= data.size.toDouble()
        return Math.sqrt(ms)
    }

    private fun setWaveFileHeader(file: File, channels: Int) {
        val fileSize = file.length() - 8
        val totalSize = fileSize + 36
        val byteRate: Long =
            (RECORDER_SAMPLE_RATE * channels * (8 / 8)).toLong() //2 byte per 1 sample for 1 channel.
        try {
            val wavFile: RandomAccessFile = randomAccessFile(file)
            wavFile.seek(0) // to the beginning
            wavFile.write(generateHeader(fileSize,
                totalSize,
                RECORDER_SAMPLE_RATE.toLong(),
                channels,
                byteRate))
            wavFile.close()
        } catch (e: FileNotFoundException) {

        } catch (e: IOException) {

        }
    }

    private fun randomAccessFile(file: File): RandomAccessFile {
        val randomAccessFile: RandomAccessFile
        randomAccessFile = try {
            RandomAccessFile(file, "rw")
        } catch (e: FileNotFoundException) {
            throw RuntimeException(e)
        }
        return randomAccessFile
    }

    private fun generateHeader(
        totalAudioLen: Long, totalDataLen: Long, longSampleRate: Long, channels: Int,
        byteRate: Long,
    ): ByteArray? {
        val header = ByteArray(44)
        header[0] = 'R'.toByte() // RIFF/WAVE header
        header[1] = 'I'.toByte()
        header[2] = 'F'.toByte()
        header[3] = 'F'.toByte()
        header[4] = (totalDataLen and 0xff).toByte()
        header[5] = (totalDataLen shr 8 and 0xff).toByte()
        header[6] = (totalDataLen shr 16 and 0xff).toByte()
        header[7] = (totalDataLen shr 24 and 0xff).toByte()
        header[8] = 'W'.toByte()
        header[9] = 'A'.toByte()
        header[10] = 'V'.toByte()
        header[11] = 'E'.toByte()
        header[12] = 'f'.toByte() // 'fmt ' chunk
        header[13] = 'm'.toByte()
        header[14] = 't'.toByte()
        header[15] = ' '.toByte()
        header[16] = 16 //16 for PCM. 4 bytes: size of 'fmt ' chunk
        header[17] = 0
        header[18] = 0
        header[19] = 0
        header[20] = 1 // format = 1
        header[21] = 0
        header[22] = channels.toByte()
        header[23] = 0
        header[24] = (longSampleRate and 0xff).toByte()
        header[25] = (longSampleRate shr 8 and 0xff).toByte()
        header[26] = (longSampleRate shr 16 and 0xff).toByte()
        header[27] = (longSampleRate shr 24 and 0xff).toByte()
        header[28] = (byteRate and 0xff).toByte()
        header[29] = (byteRate shr 8 and 0xff).toByte()
        header[30] = (byteRate shr 16 and 0xff).toByte()
        header[31] = (byteRate shr 24 and 0xff).toByte()
        header[32] = (channels * (8/ 8)) as Byte // block align
        header[33] = 0
        header[34] = 8 // bits per sample
        header[35] = 0
        header[36] = 'd'.toByte()
        header[37] = 'a'.toByte()
        header[38] = 't'.toByte()
        header[39] = 'a'.toByte()
        header[40] = (totalAudioLen and 0xff).toByte()
        header[41] = (totalAudioLen shr 8 and 0xff).toByte()
        header[42] = (totalAudioLen shr 16 and 0xff).toByte()
        header[43] = (totalAudioLen shr 24 and 0xff).toByte()
        return header
    }

    private fun writeDatatoOutputFile(BUFFER_SIZE: Int)
    {
        var i = 0
        val data = byteArrayOf(BUFFER_SIZE.toByte())
        var fOut: FileOutputStream? = null;
        try {
            fOut =FileOutputStream(fileutility.outPutFile);
        } catch (e: FileNotFoundException) {
            e.printStackTrace();
            Log.e("TAG", "writeDatatoOutputFile: "+ e.message )
        }
        while (isRecordingAudio) {
            val read = audioRecord?.read(data, 0, data.size)

            if(isAudible(data))
            {
                try {
                    read?.let { fOut?.write(data, 0, it) };
                } catch (e:IOException) {
                    e.printStackTrace();
                }
            }
            Log.e("TAG", "writeDatatoOutputFile: ${++i}")
        }
        try {
            fOut?.flush();
            fOut?.close();
        } catch (e:IOException) {
            e.printStackTrace();
            Log.e("TAG", "writeDatatoOutputFile: "+e.message);
        }
    }

    private fun isAudible(data: ByteArray): Boolean {
        val rms = getRootMeanSquared(data)
        Log.e("TAG", "isAudible: $rms")
        return rms > 125 &&  rms<129
    }

    private fun getRootMeanSquared(data: ByteArray): Double {
        var ms = 0.0
        for (i in data.indices) {
            ms += (data[i] * data[i]).toDouble()
        }
        ms /= data.size.toDouble()
        return Math.sqrt(ms)
    }*/
}