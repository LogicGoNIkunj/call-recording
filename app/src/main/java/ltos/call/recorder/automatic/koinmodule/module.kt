package ltos.call.recorder.automatic.utility

import ltos.call.recorder.automatic.singleton.AutoStartHelper
import ltos.call.recorder.automatic.singleton.FileUtility
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module


val signletoneModule: Module = module {
    single { MySharedPrefernce(androidContext()) }
    single { AutoStartHelper() }
    single { FileUtility() }
}