package ltos.call.recorder.automatic.singleton

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.audiofx.LoudnessEnhancer
import ltos.call.recorder.automatic.iinterface.PlayerToView
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object MyMediaPlayer:KoinComponent {

    private val myPref: MySharedPrefernce? by inject()

    private var mediaPlayer: MediaPlayer? = null
    private var loudness: LoudnessEnhancer?= null
    private var completelistener:MediaPlayer.OnCompletionListener? = null
    var playerToview:PlayerToView? = null

    fun createAudioManager(context: Context)
    {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager.setParameters("noise_suppression=auto")

        completelistener = MediaPlayer.OnCompletionListener { mp->
            mp.seekTo(0)
            playerToview?.playerOnComplete()
        }
    }

    fun setMediaPlayer(){
        mediaPlayer = MediaPlayer()
        mediaPlayer?.setDataSource(TempData.currentFilepath)
        mediaPlayer?.setOnCompletionListener(completelistener)
        mediaPlayer?.prepare()
        loudness = mediaPlayer?.audioSessionId?.let { LoudnessEnhancer(it).apply {
            enabled = true
        } }
    }

    fun startMediaPlayer()
    {
        mediaPlayer?.start()
        loudness?.setTargetGain((myPref?.getString("loudnessValue")?.toInt()?:0) * 250)

    }

    fun seekToMediaPlayer(i: Int)
    {
        mediaPlayer?.seekTo(i)
    }

    fun setLoudness(progress:Int)
    {
        loudness?.setTargetGain(progress)
    }

    fun pauseMediaPlayer()
    {
        mediaPlayer?.pause()
    }

    fun resumeMediaPlayer()
    {
        mediaPlayer?.start()
    }

    fun stopMediaPlayer()
    {
        mediaPlayer?.let {
            it.stop()
            it.release()
        }
        loudness?.release()
        mediaPlayer = null
        loudness = null
    }
}