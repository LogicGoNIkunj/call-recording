package ltos.call.recorder.automatic.api

import ltos.call.recorder.automatic.dataclass.Feedback
import ltos.call.recorder.automatic.dataclass.Model_Ads
import ltos.call.recorder.automatic.dataclass.Model_Playdata
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface RetrofitInterface {
    @FormUrlEncoded
    @POST("feedback")
    fun sendfeedback(
        @Field("app_name") app_name: String?,
        @Field("package_name") package_name: String?,
        @Field("title") title: String?,
        @Field("description") description: String?,
        @Field("device_name") device_name: String?,
        @Field("android_version") android_version: String?,
        @Field("version") version: String?
    ): Call<Feedback?>?


    @FormUrlEncoded
    @POST("get-ads-list")
    fun getAdId(@Field("app_id") IntVal: Int): Call<Model_Ads?>?

    @GET("qureka-ad")
    fun getBtnAd(): Call<Model_Playdata>?
}