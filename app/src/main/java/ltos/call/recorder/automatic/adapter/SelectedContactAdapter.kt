package ltos.call.recorder.automatic.adapter

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.utility.MyContact
import ltos.call.recorder.automatic.databinding.SingleContactlayoutBinding
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

class SelectedContactAdapter(val from: String) : RecyclerView.Adapter<SelectedContactAdapter.ViewHolder>(),
    KoinComponent {

    private val myPref: MySharedPrefernce by inject()
    var contactList: MutableList<MyContact>? = null
    private var context: Context? = null

    init {
        contactList = myPref.getContactList(from)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(SingleContactlayoutBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentContact = contactList?.get(position)
        holder.binding.displayname.text = currentContact?.displayName
        holder.binding.phonenumber.text = currentContact?.phoneNumber

        holder.binding.delete.setOnClickListener {
            showDeleteDialog(position)
        }
    }

    private fun showDeleteDialog(position: Int) {

        AlertDialog.Builder(context!!)
            .setTitle("Delete")
            .setMessage("Are you sure want to delete?")
            .setPositiveButton("Yes", DialogInterface.OnClickListener { dialogInterface, i ->
                updateContectList(position)
            })
            .setNegativeButton("No", DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
            })
            .setCancelable(false)
            .create()
            .show()
    }

    private fun updateContectList(position: Int)
    {
        notifyItemRemoved(position)
        val tempList = LinkedList(contactList)
        tempList.removeAt(position)
        contactList = tempList
        notifyItemRangeChanged(position, contactList!!.size)
    }

    override fun getItemCount(): Int {
        return contactList?.size?:0
    }

    class ViewHolder(var binding: SingleContactlayoutBinding) : RecyclerView.ViewHolder(binding.root)
}