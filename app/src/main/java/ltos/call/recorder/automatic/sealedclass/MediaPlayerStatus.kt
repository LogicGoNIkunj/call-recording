package ltos.call.recorder.automatic.sealedclass

sealed class MediaPlayerStatus{
    object pause:MediaPlayerStatus()
    object play:MediaPlayerStatus()
    object empty:MediaPlayerStatus()
}
