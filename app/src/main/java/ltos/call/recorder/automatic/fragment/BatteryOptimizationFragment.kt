package ltos.call.recorder.automatic.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.FragmentBatteryOptimizationBinding


class BatteryOptimizationFragment : Fragment() {

    private var binding: FragmentBatteryOptimizationBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBatteryOptimizationBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setAllClick()
    }

    private fun setAllClick() {

        binding?.nextBtn?.setOnClickListener {
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.fragmentContainer, AutoStartFragment(), null)
                ?.addToBackStack(null)
                ?.commit()
        }

        binding?.prevBtn?.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            {
                requireActivity().supportFragmentManager.popBackStack()
            }
            else{
                requireActivity().finish()
            }
        }

        binding?.permissionButton?.setOnClickListener {
            if(!isIgnoringBatteryOptimizations(requireContext()))
            {
                batteryOptimization
            }
            else
            {
                Toast.makeText(
                    context,
                    "Battery optimization Permission already Granted",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }



    val batteryOptimization: Unit
        get() {
            /*try {
                val intent = Intent()
                intent.action = Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS
                startActivity(intent)
            } catch (e: Exception) {
                Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }*/
            try {
                val intent = Intent()
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                intent.setData(Uri.parse("package:" + requireContext().packageName))
                startActivity(intent)
            } catch (e: Exception) {
            }
        }

    fun isIgnoringBatteryOptimizations(context: Context): Boolean {
        val pwrm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val name = context.packageName
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return pwrm.isIgnoringBatteryOptimizations(name)
        }
        return true
    }

}