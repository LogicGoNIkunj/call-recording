package ltos.call.recorder.automatic.dataclass

data class Feedback(val status: Boolean, val message: String)

