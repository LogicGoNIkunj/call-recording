package ltos.call.recorder.automatic.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ltos.call.recorder.automatic.activity.MainActivity
import ltos.call.recorder.automatic.databinding.FragmentAutoStartBinding
import ltos.call.recorder.automatic.koinmodule.MyApplication
import ltos.call.recorder.automatic.singleton.AutoStartHelper
import org.koin.android.ext.android.inject

class AutoStartFragment : Fragment() {

    private var binding:FragmentAutoStartBinding? = null
    private val autoStartHelper: AutoStartHelper by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAutoStartBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAllClick()
    }

    private fun setAllClick() {

        binding?.nextBtn?.setOnClickListener {
            MyApplication.appOpenManager!!.isAdShow = false
            startActivity(Intent(requireContext(), MainActivity::class.java))
            requireActivity().finish()
        }

        binding?.prevBtn?.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }

        binding?.permissionButton?.setOnClickListener {
            autoStartHelper.getAutoStartPermission(requireContext())
        }
    }

}