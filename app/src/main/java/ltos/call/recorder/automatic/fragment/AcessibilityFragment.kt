package ltos.call.recorder.automatic.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.FragmentAcessibilityBinding
import ltos.call.recorder.automatic.services.MyAcessibilityService
import java.util.*


class AcessibilityFragment : Fragment() {

    private var binding: FragmentAcessibilityBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentAcessibilityBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSpammableText()
        setAllClick()

    }

    private fun setAllClick() {
        binding?.nextBtn?.setOnClickListener {
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.fragmentContainer, BatteryOptimizationFragment(), null)
                ?.addToBackStack(null)
                ?.commit()
        }

        binding?.prevBtn?.setOnClickListener {
            requireActivity().finish()
        }

        binding?.permissionButton?.setOnClickListener {
            if(!isAccessServiceEnabled(context))
            {
                permissionAcessibility
            }
            else
            {
                Toast.makeText(
                    context,
                    "Acessibility Permission already granted",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun setSpammableText() {
        var link = ""
        val build_info = Build.BRAND.lowercase(Locale.getDefault())
        when(build_info)
        {
            "xiaomi"-> link = "https://www.youtube.com/watch?v=aQwPsIHD7VQ"
            "samsung"-> link = "https://www.youtube.com/watch?v=uD6ckCcHDHo"
            "huawei"-> link = "https://www.youtube.com/watch?v=0WgWAQW2Yuo"
            "pixel"-> link = "https://www.youtube.com/watch?v=mEtg3UO7vv4"
            else -> link = "https://www.youtube.com/watch?v=aQwPsIHD7VQ"
        }

        val clickableMindOrks = object : ClickableSpan() {
            override fun onClick(view: View) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(link)
                    )
                )
            }
        }
        val spannable = SpannableStringBuilder(getString(R.string.aceessibilitylink))

        spannable.setSpan(
            clickableMindOrks,
            11,
            30,
            0
        )

        spannable.setSpan(
            ForegroundColorSpan(Color.RED),
            11, // start
            30, // end
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )

        spannable.setSpan(
            UnderlineSpan(),
            11, // start
            30, // end
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )

        binding?.linktext?.setText(spannable, TextView.BufferType.SPANNABLE)
        binding?.linktext?.setMovementMethod(LinkMovementMethod.getInstance())
    }

    val permissionAcessibility: Unit
        get() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val openSettings = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                openSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY)
                startActivity(openSettings)
            } else {
                Toast.makeText(
                    context,
                    "Acessibility Permission already granted",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    fun isAccessServiceEnabled(context: Context?): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val prefString = Settings.Secure.getString(
                context?.contentResolver,
                Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
            )
            prefString != null && prefString.contains(context?.packageName + "/" + MyAcessibilityService::class.java.canonicalName)
        } else {
            true
        }
    }
}