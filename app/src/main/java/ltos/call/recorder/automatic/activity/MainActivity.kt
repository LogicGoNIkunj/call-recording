package ltos.call.recorder.automatic.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.SearchView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.onesignal.OneSignal
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.activity.SplashActivity.Companion.publisher_yes
import ltos.call.recorder.automatic.adapter.CallHistoryAdapter
import ltos.call.recorder.automatic.databinding.*
import ltos.call.recorder.automatic.iinterface.PowerToActivity
import ltos.call.recorder.automatic.koinmodule.MyApplication
import ltos.call.recorder.automatic.koinmodule.MyApplication.Companion.checkqureka
import ltos.call.recorder.automatic.koinmodule.MyApplication.Companion.isNetworkAvailable
import ltos.call.recorder.automatic.koinmodule.MyApplication.Companion.qurekaimage
import ltos.call.recorder.automatic.koinmodule.MyApplication.Companion.url
import ltos.call.recorder.automatic.services.AllTimeNotificationService
import ltos.call.recorder.automatic.services.MyAcessibilityService
import ltos.call.recorder.automatic.services.RecorderOnOffService
import ltos.call.recorder.automatic.singleton.FileUtility
import ltos.call.recorder.automatic.singleton.MyMediaPlayer
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.singleton.TempData
import org.koin.android.ext.android.inject
import java.util.*

class MainActivity : AppCompatActivity(), PowerToActivity {
    private val myPref: MySharedPrefernce by inject()
    private var binding: ActivityMainBinding? = null
    private var callHistoryAdapter: CallHistoryAdapter? = null
    private var exitBinding: FragmentExitBinding? = null
    private var exitDialog: AlertDialog? = null
    private val fileutility: FileUtility? by inject()
    private var mLastClickTime = 0L

    //color
    private var red: ColorStateList? = null
    private var white: ColorStateList? = null
    private var black: ColorStateList? = null


    //Ad variable
    private var nativeAd: NativeAd? = null
    private var smallnativeAd: NativeAd? = null
    private var mInterstitialAd: InterstitialAd? = null
    private var isActivityLeft = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        Analytics()
        myPref.setString("isFirstTime", "false")
        setsmallNativeAd()
        sortOutNotification()
        setAllClick()
        startService(Intent(this, RecorderOnOffService::class.java))
        MyMediaPlayer.createAudioManager(this)
        createExitDialog()
        if (isNetworkAvailable()) {
            if (publisher_yes) {
                if (checkqureka) {
                    if (MyApplication.get_qureka()) {
                        if (qurekaimage != "") {
                            Glide.with(this).load(qurekaimage)
                                .into((findViewById<View>(R.id.img) as ImageView))
                            findViewById<View>(R.id.qurekaads).visibility = View.VISIBLE
                            findViewById<View>(R.id.qureka).setOnClickListener {
                                try {
                                    val builder = CustomTabsIntent.Builder()
                                    val customTabsIntent = builder.build()
                                    customTabsIntent.intent.setPackage("com.android.chrome")
                                    customTabsIntent.launchUrl(this, Uri.parse(url))
                                } catch (e: Exception) {
                                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT)
                                        .show()
                                }
                            }
                        } else {
                            findViewById<View>(R.id.img).visibility = View.GONE
                            findViewById<View>(R.id.qurekaads).visibility = View.GONE
                        }
                    }

                }
            }
        } else {
            findViewById<View>(R.id.img).visibility = View.GONE
            findViewById<View>(R.id.qurekaads).visibility = View.GONE
        }
    }

    private fun Analytics() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        FirebaseCrashlytics.getInstance()
        FirebaseAnalytics.getInstance(this)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(resources.getString(R.string.onesignal))
    }

    private fun sortOutNotification() {
        if (myPref.getString("isNotificationOn").toBoolean()) {
            val i = Intent(this, AllTimeNotificationService::class.java)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(i)
            } else {
                startService(i)
            }
        }
    }

    private fun setAcessibilityText() {
        if (isAccessServiceEnabled()) {
            binding?.AccesibilityError?.visibility = View.GONE
            binding?.turnOn?.visibility = View.GONE
        } else {
            binding?.AccesibilityError?.visibility = View.VISIBLE
            binding?.turnOn?.visibility = View.VISIBLE
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setAllClick() {

        binding?.settingbtn?.setOnClickListener {
            startActivity(Intent(this@MainActivity, SettingActivity::class.java))
        }

        binding?.searchbtn?.setOnClickListener {
            binding?.qureka?.visibility = View.GONE
            binding?.titleText?.visibility = View.GONE
            binding?.searchbtn?.visibility = View.GONE
            binding?.settingbtn?.visibility = View.GONE

            binding?.searchView?.visibility = View.VISIBLE

            binding?.searchView?.isFocusable = true
            binding?.searchView?.isIconified = false
            binding?.searchView?.requestFocusFromTouch()
        }

        binding?.searchView?.setOnCloseListener {
            binding?.qureka?.visibility = View.VISIBLE
            binding?.titleText?.visibility = View.VISIBLE
            binding?.searchbtn?.visibility = View.VISIBLE
            binding?.settingbtn?.visibility = View.VISIBLE

            binding?.searchView?.visibility = View.INVISIBLE

            true
        }

        binding?.searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onQueryTextChange(query: String): Boolean {
                callHistoryAdapter?.fileList = fileutility?.allCallRecodinglist?.filter {
                    it.name.contains(query)
                }
                callHistoryAdapter?.notifyDataSetChanged()
                return false
            }
        })

        red = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.custom_red))
        white = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white))
        black = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black))

        binding?.allbtn?.setOnClickListener {
            callHistoryAdapter?.fileList = fileutility?.allCallRecodinglist
            when (callHistoryAdapter?.fileList?.size) {
                0, null -> {
                    binding?.emptyTextview?.visibility = View.VISIBLE
                }
                else -> {

                    binding?.emptyTextview?.visibility = View.GONE
                }
            }
            callHistoryAdapter?.notifyDataSetChanged()
            selectAllbtn()
        }

        binding?.incomingbtn?.setOnClickListener {
            callHistoryAdapter?.fileList = fileutility?.allCallRecodinglist?.filter {
                it.name.contains("Incoming")
            }
            when (callHistoryAdapter?.fileList?.size) {
                0, null -> {
                    binding?.emptyTextview?.visibility = View.VISIBLE
                }
                else -> {
                    binding?.emptyTextview?.visibility = View.GONE
                }
            }
            callHistoryAdapter?.notifyDataSetChanged()
            selectIncomingbtn()
        }

        binding?.outgoingbtn?.setOnClickListener {
            callHistoryAdapter?.fileList = fileutility?.allCallRecodinglist?.filter {
                it.name.contains("Outgoing")
            }
            when (callHistoryAdapter?.fileList?.size) {
                0, null -> {
                    binding?.emptyTextview?.visibility = View.VISIBLE
                }
                else -> {
                    binding?.emptyTextview?.visibility = View.GONE
                }
            }
            callHistoryAdapter?.notifyDataSetChanged()
            selectOutgoingbtn()
        }

        binding?.turnOn?.setOnClickListener {
            myPref.setString("isFirstTime", "true")
            startActivity(Intent(this, PermissionOneActivity::class.java))
        }
    }

    private fun selectIncomingbtn() {
        binding?.allbtn?.backgroundTintList = null
        binding?.incomingbtn?.backgroundTintList = red
        binding?.outgoingbtn?.backgroundTintList = null

        binding?.allbtn?.setTextColor(black)
        binding?.incomingbtn?.setTextColor(white)
        binding?.outgoingbtn?.setTextColor(black)
    }

    private fun selectOutgoingbtn() {
        binding?.allbtn?.backgroundTintList = null
        binding?.incomingbtn?.backgroundTintList = null
        binding?.outgoingbtn?.backgroundTintList = red

        binding?.allbtn?.setTextColor(black)
        binding?.incomingbtn?.setTextColor(black)
        binding?.outgoingbtn?.setTextColor(white)
    }

    private fun selectAllbtn() {
        binding?.allbtn?.backgroundTintList = red
        binding?.incomingbtn?.backgroundTintList = null
        binding?.outgoingbtn?.backgroundTintList = null

        binding?.allbtn?.setTextColor(white)
        binding?.incomingbtn?.setTextColor(black)
        binding?.outgoingbtn?.setTextColor(black)
    }

    fun setupRecyclerView() {
        callHistoryAdapter = CallHistoryAdapter()
        when (callHistoryAdapter?.fileList?.size) {
            null, 0 -> {
            }
            else -> binding?.emptyTextview?.visibility = View.GONE
        }
        callHistoryAdapter?.listener = this
        binding?.recyclerView?.apply {
            adapter = callHistoryAdapter
        }
    }

    private fun createExitDialog() {
        if (exitBinding == null) {
            exitBinding = FragmentExitBinding.inflate(layoutInflater)
            exitBinding?.btnPositive?.setOnClickListener {
                MyApplication.appOpenManager?.isAdShow = true
                exitDialog?.dismiss()
                finishAffinity()
            }
            exitBinding?.btnNegative?.setOnClickListener {
                exitDialog?.dismiss()
            }


            exitDialog = AlertDialog.Builder(this)
                .setView(exitBinding?.root)
                .setCancelable(false)
                .create()
                .apply {
                    window?.setBackgroundDrawable(
                        AppCompatResources.getDrawable(
                            this@MainActivity,
                            R.drawable.roundcorner_bg
                        )
                    )
                }

            setNativeAd()
        }
    }

    private fun setNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (nativeAd != null) {
                nativeAd?.destroy()
            }
            nativeAd = unifiedNativeAd
            val adView = UnifiednativeadBinding.inflate(layoutInflater).root
            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            exitBinding?.adlayout?.removeAllViews()
            exitBinding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                exitBinding?.defaultImage?.visibility = View.GONE
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                exitBinding?.defaultImage?.visibility = View.VISIBLE
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.mediaView = mediaView
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.visibility = View.INVISIBLE
        } else {
            adView.bodyView?.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.visibility = View.INVISIBLE
        } else {
            adView.callToActionView?.visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon?.drawable
            )
            adView.iconView?.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }


    private fun showExitDialog() {
        exitDialog?.show()
    }

    override fun deletefile() {
        showDeleteDialog()
    }

    override fun gotoMediaPlayerActivity() {
        startActivity(Intent(this, MediaPlayerActivity::class.java))
        showInterstaticial()
    }

    fun showDeleteDialog() {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val intent = MediaStore.createDeleteRequest(contentResolver, setOf(TempData.currentUri))
            try {
                startIntentSenderForResult(intent.intentSender, 1, null, 0, 0, 0, null)
            } catch (e: IntentSender.SendIntentException) {
                e.printStackTrace()
            }
        } else {
            val deletedialogBinding = DeletedialogBinding.inflate(layoutInflater)
            val deletedialog = AlertDialog.Builder(this)
                .setView(deletedialogBinding.root)
                .create()
            deletedialog.window?.setBackgroundDrawable(
                AppCompatResources.getDrawable(
                    this,
                    R.drawable.roundcorner_bg
                )
            )
            deletedialog.show()
            deletedialog.getWindow()?.setLayout(
                (resources.displayMetrics.widthPixels * 0.8).toInt(),
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            deletedialogBinding.nobtn.setOnClickListener {
                deletedialog.dismiss()
            }
            deletedialogBinding.yesbtn.setOnClickListener {
                normaldeletefile()
                deletedialog.dismiss()
            }
        }
    }

    private fun normaldeletefile() {

        if (fileutility?.allCallRecodinglist?.get(TempData.currentposition)?.delete() == true) {
            updateAdapterAfterDelete()
        } else {
            Toast.makeText(this, "Delete operation failed!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateAdapterAfterDelete() {
        callHistoryAdapter?.notifyItemRemoved(TempData.currentposition)
        val tempList = LinkedList(callHistoryAdapter?.fileList)
        tempList.removeAt(TempData.currentposition)
        callHistoryAdapter?.fileList = tempList
        fileutility?.allCallRecodinglist = tempList
        callHistoryAdapter?.notifyItemRangeChanged(
            TempData.currentposition,
            callHistoryAdapter?.fileList?.size!!
        )
        when (callHistoryAdapter?.fileList?.size) {
            0, null -> {
                binding?.emptyTextview?.visibility = View.VISIBLE
            }
            else -> {
                binding?.emptyTextview?.visibility = View.GONE
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK) {
            updateAdapterAfterDelete()
        }
    }

    private fun LoadIntersstaticialAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.fullScreenContentCallback = object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            super.onAdDismissedFullScreenContent()
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            super.onAdFailedToShowFullScreenContent(adError)
                            MyApplication.appOpenManager!!.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            MyApplication.appOpenManager!!.isAdShow = true
                        }
                    }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    MyApplication.appOpenManager!!.isAdShow = false
                }
            })
    }

    private fun showInterstaticial() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd!!.show(this)
                MyApplication.appOpenManager!!.isAdShow = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun isAccessServiceEnabled(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val prefString = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
            prefString != null && prefString.contains(packageName + "/" + MyAcessibilityService::class.java.canonicalName)
        } else {
            true
        }
    }

    private fun setsmallNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (smallnativeAd != null) {
                smallnativeAd?.destroy()
            }
            smallnativeAd = unifiedNativeAd
            val adView = SmallNativeBinding.inflate(layoutInflater).root
            populateUnifiedSmallNativeAdView(unifiedNativeAd, adView)
            binding?.adlayout?.removeAllViews()
            binding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding?.adtext?.visibility = View.GONE
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                binding?.adtext?.visibility = View.GONE
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    fun populateUnifiedSmallNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.visibility = View.INVISIBLE
        } else {
            adView.bodyView?.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.visibility = View.INVISIBLE
        } else {
            adView.callToActionView?.visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }

    override fun onStart() {
        super.onStart()
        isActivityLeft = false
    }

    override fun onResume() {
        super.onResume()
        isActivityLeft = false
        setAcessibilityText()
        setupRecyclerView()
        LoadIntersstaticialAd()
        selectAllbtn()
    }

    override fun onPause() {
        super.onPause()
        isActivityLeft = true
    }

    override fun onStop() {
        super.onStop()
        isActivityLeft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        if (nativeAd != null) {
            nativeAd?.destroy()
        }
        if (smallnativeAd != null) {
            nativeAd?.destroy()
        }
        isActivityLeft = true
    }

    override fun onBackPressed() {
        showExitDialog()
    }
}