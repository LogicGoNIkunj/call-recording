package ltos.call.recorder.automatic.activity

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.ActivityContactBinding
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import org.koin.android.ext.android.inject

class ContactActivity : AppCompatActivity() {

    private var binding: ActivityContactBinding? = null
    private val myPref: MySharedPrefernce by inject()
    private var red:ColorStateList? = null
    private var lightgrey:ColorStateList? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactBinding.inflate(layoutInflater)

        red = ColorStateList.valueOf(resources.getColor(R.color.custom_red))
        lightgrey = ColorStateList.valueOf(resources.getColor(R.color.medium_grey))

        setContentView(binding?.root)
        setContactSwitch()
        setAllClick()
    }

    private fun setAllClick() {
        binding?.contactRecord?.setOnClickListener {
            startActivity(Intent(this, SelectedContactActivity::class.java).apply {
                putExtra(
                    "for",
                    "contactRecord"
                )
            })
        }
        binding?.contactIgnore?.setOnClickListener {
            startActivity(Intent(this, SelectedContactActivity::class.java).apply {
                putExtra(
                    "for",
                    "contactIgnore"
                )
            })
        }

        binding?.backbtn?.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setContactSwitch() {

        when (myPref.getString("isContact")) {
            "true" -> {
                binding?.recordContact?.isChecked = true
                binding?.contactRecord?.isEnabled = false
                /*binding?.contactRecord?.setBackgroundColor(
                    ContextCompat.getColor(
                        this@ContactActivity,
                        R.color.medium_grey
                    )
                )
                binding?.contactIgnore?.setBackgroundColor(
                    ContextCompat.getColor(
                        this@ContactActivity,
                        R.color.custom_red
                    )
                )*/
                binding?.contactIgnore?.backgroundTintList = red
                binding?.contactRecord?.backgroundTintList = lightgrey
            }
            "false" -> {
                binding?.recordContact?.isChecked = false
                binding?.contactIgnore?.isEnabled = false
                /*binding?.contactRecord?.setBackgroundColor(
                    ContextCompat.getColor(
                        this@ContactActivity,
                        R.color.custom_red
                    )
                )
                binding?.contactIgnore?.setBackgroundColor(
                    ContextCompat.getColor(
                        this@ContactActivity,
                        R.color.medium_grey
                    )
                )*/
                binding?.contactIgnore?.backgroundTintList = lightgrey
                binding?.contactRecord?.backgroundTintList = red
            }
        }
        binding?.recordContact?.setOnCheckedChangeListener(object :
            CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(cb: CompoundButton?, boolean: Boolean) {
                when (boolean) {
                    true -> {
                        myPref.setString("isContact", "true")
                        binding?.contactRecord?.isEnabled = false
                        binding?.contactIgnore?.isEnabled = true
                       /* binding?.contactRecord?.setBackgroundColor(
                            ContextCompat.getColor(
                                this@ContactActivity,
                                R.color.medium_grey
                            )
                        )
                        binding?.contactIgnore?.setBackgroundColor(
                            ContextCompat.getColor(
                                this@ContactActivity,
                                R.color.custom_red
                            )
                        )*/
                        binding?.contactIgnore?.backgroundTintList = red
                        binding?.contactRecord?.backgroundTintList = lightgrey
                    }
                    false -> {
                        myPref.setString("isContact", "false")
                        binding?.contactRecord?.isEnabled = true
                        binding?.contactIgnore?.isEnabled = false
                       /* binding?.contactRecord?.setBackgroundColor(
                            ContextCompat.getColor(
                                this@ContactActivity,
                                R.color.custom_red
                            )
                        )
                        binding?.contactIgnore?.setBackgroundColor(
                            ContextCompat.getColor(
                                this@ContactActivity,
                                R.color.medium_grey
                            )
                        )*/
                        binding?.contactIgnore?.backgroundTintList = lightgrey
                        binding?.contactRecord?.backgroundTintList = red
                    }
                }
            }
        })
    }
}