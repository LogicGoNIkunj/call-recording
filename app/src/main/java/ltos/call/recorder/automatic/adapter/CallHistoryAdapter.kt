package ltos.call.recorder.automatic.adapter

import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaScannerConnection
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.SingleCallhistoryBinding
import ltos.call.recorder.automatic.iinterface.PowerToActivity
import ltos.call.recorder.automatic.receiver.CallStatusReceiver
import ltos.call.recorder.automatic.singleton.FileUtility
import ltos.call.recorder.automatic.singleton.TempData
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class CallHistoryAdapter: RecyclerView.Adapter<CallHistoryAdapter.ViewHolder>(), KoinComponent {

    private val fileUtility:FileUtility by inject()
    var fileList:List<File>? = null
    private var context: Context? = null
    var listener:PowerToActivity? = null

    init {
        fileList = fileUtility.getAllRecording()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(SingleCallhistoryBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(position == 0 && CallStatusReceiver.serviceStarted == 0 )
        {
            holder.binding.filename.text = "Call recorder running...."
            holder.binding.filecreatedtime.text = "00:00"
            return
        }

        val currentFile = fileList?.get(position)
        holder.binding.filename.text = currentFile?.name
        holder.binding.filecreatedtime.text = currentFile?.lastModified()?.let { convertLongToTime(it) }

        currentFile?.path?.let{ currentFilepath ->
            holder.binding.playbtn.setOnClickListener {
                TempData.endTime = getFileDuration(currentFile.path)
                TempData.currentFilepath = currentFilepath
                TempData.currentfilename = currentFile.name
                listener?.gotoMediaPlayerActivity()
            }
        }

        holder.binding.menu.setOnClickListener {

            MediaScannerConnection.scanFile(context, arrayOf(currentFile?.path), null) { s, uri -> // uri is in format content://...
                CoroutineScope(Dispatchers.Main).launch {
                    TempData.currentUri = uri
                    TempData.currentposition = position
                }
            }
            context?.let { it1 ->
                val popup = PopupMenu(it1, holder.binding.menu)
                popup.menuInflater.inflate(
                    R.menu.mainmenu
                    , popup.menu)
                popup.setForceShowIcon(true)
                popup.setOnMenuItemClickListener {
                    when(it.itemId)
                    {
                        R.id.deletemenu-> listener?.deletefile()
                        R.id.sharemenu-> sharemore()
                    }
                    true
                }
                popup.show()
            }
        }
    }

    override fun getItemCount(): Int {
        return fileList?.size?:0
    }

    class ViewHolder( val binding: SingleCallhistoryBinding) :RecyclerView.ViewHolder(binding.root)

    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd.MM.yyyy (HH:mm)")
        return format.format(date)
    }

    private fun sharemore() {
        try {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "audio/*"
            share.putExtra(Intent.EXTRA_STREAM, TempData.currentUri)
            context?.startActivity(share)
        } catch (e: Exception) {
        }
    }

    fun getFileDuration(currentFilePath: String): String {
        try {
            val retriever = MediaMetadataRetriever()
            retriever.setDataSource(currentFilePath)
            val videoDuationInString =
                retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toString()
            retriever.release()
            return videoDuationInString
        } catch (e: Exception) {
            return "00"
        }
    }
}