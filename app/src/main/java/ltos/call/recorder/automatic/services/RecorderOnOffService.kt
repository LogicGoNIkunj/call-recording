package ltos.call.recorder.automatic.services

import android.app.Service
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log
import ltos.call.recorder.automatic.receiver.CallStatusReceiver
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import org.koin.android.ext.android.inject

class RecorderOnOffService:Service() {

    private var callStatusReceiver:CallStatusReceiver? = null
    private val myPref: MySharedPrefernce by inject()

    override fun onCreate() {
        super.onCreate()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        when (myPref.getString("isRecordingOn")) {
            "true" -> registerCallReceiver()
            "false" -> unregisterCallReceiver()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    fun registerCallReceiver() {
        callStatusReceiver = CallStatusReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.intent.action.PHONE_STATE")
        intentFilter.addAction("android.intent.action.NEW_OUTGOING_CALL")
        registerReceiver(callStatusReceiver, intentFilter)
    }

    fun unregisterCallReceiver()
    {
        try {
            unregisterReceiver(callStatusReceiver)
        }
        catch (e:Exception)
        {
        }
    }
}