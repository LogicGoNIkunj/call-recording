package ltos.call.recorder.automatic.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import kotlinx.coroutines.*
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.ActivityMediaPlayerBinding
import ltos.call.recorder.automatic.databinding.UnifiednativeadBinding
import ltos.call.recorder.automatic.iinterface.PlayerToView
import ltos.call.recorder.automatic.koinmodule.MyApplication
import ltos.call.recorder.automatic.sealedclass.MediaPlayerStatus
import ltos.call.recorder.automatic.singleton.MyMediaPlayer
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.singleton.TempData
import org.koin.android.ext.android.inject

class MediaPlayerActivity : AppCompatActivity(), PlayerToView {

    private val myPref: MySharedPrefernce by inject()
    private var binding: ActivityMediaPlayerBinding? = null
    private var mediaPlayerstatus: MediaPlayerStatus? = null
    private var job: Job? = null

    //Ad Variable
    private var nativeAd: NativeAd? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMediaPlayerBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        binding?.filename?.text = TempData.currentfilename

        setMediaPlayerParameters()
        setAllSeekBar()
        setAllClick()
        MyMediaPlayer.setMediaPlayer()
        setNativeAd()
    }

    fun setNativeAd() {
        val builder = AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
        builder.forNativeAd { unifiedNativeAd: NativeAd ->
            if (nativeAd != null) {
                nativeAd?.destroy()
            }
            nativeAd = unifiedNativeAd
            val adView = UnifiednativeadBinding.inflate(layoutInflater).root
            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            binding?.adlayout?.removeAllViews()
            binding?.adlayout?.addView(adView)
        }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding?.defaultImage?.visibility = View.GONE
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                binding?.defaultImage?.setVisibility(View.VISIBLE)
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.setMediaView(mediaView)
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline))
        adView.setBodyView(adView.findViewById(R.id.ad_body))
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action))
        adView.setIconView(adView.findViewById(R.id.ad_app_icon))
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.visibility = View.INVISIBLE
        } else {
            adView.bodyView?.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.visibility = View.INVISIBLE
        } else {
            adView.callToActionView?.visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon?.drawable
            )
            adView.iconView?.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }


    private fun setAllSeekBar() {
        setLoudnessSeekBar()
        setDurationSeekbarOne()
        setDurationSeekbarTwo()
        MyMediaPlayer.playerToview = this
    }

    @SuppressLint("SetTextI18n")
    private fun setMediaPlayerParameters() {
        mediaPlayerstatus = MediaPlayerStatus.empty
        setEndTime()
        binding?.startTime?.text = "0:00:00"
        binding?.pauseplaybtn?.setImageResource(R.drawable.ic_playone)
    }

    private fun setAllClick() {
        binding?.pauseplaybtn?.setOnClickListener {
            when (mediaPlayerstatus) {
                MediaPlayerStatus.empty -> {
                    mediaPlayerstatus = MediaPlayerStatus.play
                    MyMediaPlayer.startMediaPlayer()
                    job?.cancel()
                    job = CoroutineScope(Dispatchers.IO).async {
                        manageTimer()
                    }
                    binding?.pauseplaybtn?.setImageResource(R.drawable.ic_pause)
                }
                MediaPlayerStatus.pause -> {
                    mediaPlayerstatus = MediaPlayerStatus.play
                    MyMediaPlayer.resumeMediaPlayer()
                    job?.cancel()
                    job = CoroutineScope(Dispatchers.IO).async {
                        manageTimer()
                    }
                    binding?.pauseplaybtn?.setImageResource(R.drawable.ic_pause)
                }
                MediaPlayerStatus.play -> {
                    pauseMediaPlayer()
                }
                else -> Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
            }
        }

        binding?.moveprev?.setOnClickListener {
            val seek_progress = (binding?.seekbarDuration?.progress)?.minus(10)
            if (seek_progress != null && seek_progress >= 0) {
                binding?.seekbarDuration?.progress = seek_progress
                MyMediaPlayer.seekToMediaPlayer(seek_progress * 1000)

                CoroutineScope(Dispatchers.IO).launch {
                    updateStartEndText(seek_progress * 1000)
                    cancel()
                }
            }
        }
        binding?.movenext?.setOnClickListener {
            val seek_progress = (binding?.seekbarDuration?.progress)?.plus(10)
            if (seek_progress != null && seek_progress <= binding?.seekbarDuration?.max!!) {
                binding?.seekbarDuration?.progress = seek_progress
                MyMediaPlayer.seekToMediaPlayer(seek_progress * 1000)

                CoroutineScope(Dispatchers.IO).launch {
                    updateStartEndText(seek_progress * 1000)
                    cancel()
                }
            }
        }

        binding?.backBtn?.setOnClickListener {
            onBackPressed()
        }
    }

    private fun pauseMediaPlayer() {
        mediaPlayerstatus = MediaPlayerStatus.pause
        MyMediaPlayer.pauseMediaPlayer()
        job?.cancel()
        binding?.pauseplaybtn?.setImageResource(R.drawable.ic_playone)
    }

    private suspend fun manageTimer() {

        while (mediaPlayerstatus?.equals(MediaPlayerStatus.play) == true) {
            delay(1000)
            val seek_progress = (binding?.seekbarDuration?.progress)?.plus(1)
            if (seek_progress != null) {
                binding?.seekbarDuration?.progress = seek_progress
            }
            updateStartEndText(seek_progress?.times(1000) ?: 0)
        }
        job?.cancel()
    }

    @SuppressLint("SetTextI18n")
    suspend fun updateStartEndText(progress: Int) {
        val remainingTime = TempData.endTime.toLong() - progress
        val minutes = (remainingTime / (1000 * 60)) % 60
        val seconds = (remainingTime / 1000) % 60
        val hours = ((remainingTime / (1000 * 60 * 60))).toInt()

        val pminutes = (progress / (1000 * 60)) % 60
        val pseconds = (progress / 1000) % 60
        val phours = ((progress / (1000 * 60 * 60)))


        withContext(Dispatchers.Main)
        {
            binding?.endTime?.text = String.format("%d", hours) + ":" + String.format(
                "%02d",
                minutes
            ) + ":" + String.format("%02d", seconds)
            binding?.startTime?.text = String.format("%d", phours) + ":" + String.format(
                "%02d",
                pminutes
            ) + ":" + String.format("%02d", pseconds)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setEndTime() {
        val milisecond = TempData.endTime.toInt()
        val minutes = (milisecond / (1000 * 60)) % 60
        val seconds = (milisecond / 1000) % 60
        val hours = (((milisecond / (1000 * 60 * 60))))
        binding?.endTime?.text =
            String.format("%d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format(
                "%02d",
                seconds
            )
    }

    private fun setDurationSeekbarOne() {
        binding?.seekbarDuration?.max = TempData.endTime.toInt() / 1000
        binding?.seekbarDuration?.progress = 0
    }

    private fun setLoudnessSeekBar() {
        binding?.seekbarLoudness?.max = 20
        binding?.seekbarLoudness?.progress = myPref.getString("loudnessValue").toInt()

        binding?.seekbarLoudness?.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(sb: SeekBar?, progress: Int, fromuser: Boolean) {
                if (fromuser) {
                    myPref.setString("loudnessValue", progress.toString())
                    MyMediaPlayer.setLoudness(progress * 250)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
    }

    private fun setDurationSeekbarTwo() {
        binding?.seekbarDuration?.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromuser: Boolean) {
                if (fromuser) {
                    MyMediaPlayer.seekToMediaPlayer(progress * 1000)
                    CoroutineScope(Dispatchers.IO).launch {
                        updateStartEndText(progress * 1000)
                        cancel()
                    }
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        MyMediaPlayer.stopMediaPlayer()
        job?.cancel()
    }

    @SuppressLint("SetTextI18n")
    override fun playerOnComplete() {
        job?.cancel()
        setMediaPlayerParameters()
        binding?.seekbarDuration?.progress = 0
        setEndTime()
        binding?.startTime?.text = "00:00"
    }

    override fun onPause() {
        super.onPause()
        when (mediaPlayerstatus) {
            MediaPlayerStatus.play -> {
                pauseMediaPlayer()
            }
            else -> {
            }
        }
    }

    override fun onStop() {
        super.onStop()
        when (mediaPlayerstatus) {
            MediaPlayerStatus.play -> {
                pauseMediaPlayer()
            }
            else -> {
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (nativeAd != null) {
            nativeAd?.destroy()
        }
    }
}