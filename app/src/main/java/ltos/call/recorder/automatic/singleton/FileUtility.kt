package ltos.call.recorder.automatic.singleton

import android.os.Environment
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class FileUtility : KoinComponent {
    var AppFolderName: File? = null
    private val myPref: MySharedPrefernce by inject()
    var allCallRecodinglist:List<File>? = null

    init {
       checkFolder()
    }

    private fun checkFolder()
    {
        AppFolderName = File(
            "${Environment.getExternalStorageDirectory()}/Download/CallRecorder11"
        )
        if (!AppFolderName!!.exists()) {
            AppFolderName?.mkdirs()
        }
    }

    val outPutFile: File
        get() {
            checkFolder()
            var CallStatus = "Incoming_"
            if (TempData.outgoingcall) {
                CallStatus = "Outgoing_"
            }
            val nOutPutFile =
                File( "$AppFolderName/$CallStatus${TempData.callnumber}_${getcurrentTime()}.${myPref.getString("AudioFormat")}")
            try {
                if (!nOutPutFile.exists()) {
                    nOutPutFile.createNewFile()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return nOutPutFile
        }

    private fun getcurrentTime(): String
    {
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault())
        return  simpleDateFormat.format(calendar.time).toString()
    }

    fun getAllRecording(): List<File>? {
        if (AppFolderName?.listFiles() != null) {
            allCallRecodinglist = Arrays.asList(*AppFolderName?.listFiles())

            if (allCallRecodinglist != null ) {
                allCallRecodinglist = allCallRecodinglist?.filter {
                    it.length() > 0 && (
                            it.name.contains("amr")
                                    || it.name.contains("3gp")
                                    || it.name.contains("aac")
                                    || it.name.contains("mp4")
                            )
                }
                (allCallRecodinglist as MutableList<File>).sortByDescending {
                    it.lastModified()
                }

                val inboxsize = myPref.getString("InboxSize").toInt()
                if(allCallRecodinglist?.size!!>inboxsize)
                {
                    for(position in inboxsize until allCallRecodinglist!!.size)
                    {
                        if(allCallRecodinglist!!.get(position).exists())
                        {
                            allCallRecodinglist!!.get(position).delete()
                        }
                    }
                }
            }
        }
        return allCallRecodinglist
    }
}