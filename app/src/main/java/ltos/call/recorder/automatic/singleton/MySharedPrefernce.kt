package ltos.call.recorder.automatic.singleton

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ltos.call.recorder.automatic.utility.MyContact

class MySharedPrefernce constructor(val context: Context) {

    var pref: SharedPreferences? = null
    var prefedit: SharedPreferences.Editor? = null

    init {
        pref = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
        prefedit = pref?.edit()
    }

     fun getString(key: String):String
     {
         when(key)
         {
             "isRecordingOn"->{return pref?.getString(key, "true")?:"true"}
             "isNotificationOn"->{return pref?.getString(key, "true")?:"true"}
             "AudioFormat"->{return pref?.getString(key, "3gp")?:"3gp"}
             "InboxSize"->{return pref?.getString(key, "100")?:"100"}
             "isContact"->{return pref?.getString(key, "true")?:"true"}
             "loudnessValue"->{return pref?.getString(key, "2")?:"2"}
             "isFirstTime"->{return pref?.getString(key, "true")?:"true"}
         }
         return pref?.getString(key, "")?:""
     }

    fun getContactList(key: String): MutableList<MyContact>{
        val contactList =  Gson().fromJson<MutableList<MyContact>>(pref?.getString(key, "") ?: "", object:TypeToken<MutableList<MyContact>>(){}.type)
        return contactList?: mutableListOf()
    }

    fun setContactList(key: String, contactList:MutableList<MyContact>){
        val value = Gson().toJson(contactList)
        prefedit?.putString(key, value)?.apply()
    }

    fun setString(key: String, value: String) {
        prefedit?.putString(key, value)?.apply()
    }
}