package ltos.call.recorder.automatic.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.ActivityPermissionOneBinding
import ltos.call.recorder.automatic.databinding.RepermissiondialogBinding
import ltos.call.recorder.automatic.fragment.ManifiestPermissionFragment
import ltos.call.recorder.automatic.koinmodule.MyApplication
import ltos.call.recorder.automatic.singleton.TempData

class PermissionOneActivity : AppCompatActivity() {

    private var binding: ActivityPermissionOneBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPermissionOneBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        MyApplication.appOpenManager!!.isAdShow = true

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, ManifiestPermissionFragment(), "null")
            .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        MyApplication.appOpenManager!!.isAdShow = false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode)
        {
            TempData.PERMISSION_MANIFIEST->{
                var permitted = true
                for(i in grantResults)
                {
                    if(i != PackageManager.PERMISSION_GRANTED)
                    {
                        permitted = false
                        if(i == PackageManager.PERMISSION_DENIED)
                        {
                            showRepermissionDialog()
                        }
                        break
                    }
                }
                if(permitted)
                {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        }
    }

    private fun showRepermissionDialog()
    {
        val repermissionbinding = RepermissiondialogBinding.inflate(layoutInflater)

        val repermissiondialog = AlertDialog.Builder(this)
            .setView(repermissionbinding.root)
            .create()


        repermissiondialog.window?.setBackgroundDrawable(AppCompatResources.getDrawable(this,R.drawable.roundcorner_bg))

        repermissiondialog.show()

        repermissiondialog.getWindow()?.setLayout((resources.displayMetrics.widthPixels*0.8).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)

        repermissionbinding.cancel.setOnClickListener {
            repermissiondialog.dismiss()
        }
        repermissionbinding.opensetting.setOnClickListener {
            try {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                intent.data = Uri.parse("package:${packageName}")
                startActivity(intent)
                repermissiondialog.dismiss()
            } catch (e: Exception) {
            }
        }
    }
}