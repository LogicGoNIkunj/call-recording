package ltos.call.recorder.automatic.koinmodule

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.gms.ads.MobileAds
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.api.AppOpenManager
import ltos.call.recorder.automatic.api.RetrofitInterface
import ltos.call.recorder.automatic.dataclass.Model_Playdata
import ltos.call.recorder.automatic.utility.signletoneModule
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class MyApplication : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null

        @SuppressLint("StaticFieldLeak")
        var appOpenManager: AppOpenManager? = null
        var preferences: SharedPreferences? = null
        var mEditor: SharedPreferences.Editor? = null

        private var retrofit: Retrofit? = null
        var qurekaimage = ""
        var querekatext: String? = ""
        var url: String? = ""
        var checkqureka = false
        fun set_AdsInt(adsInt: Int) {
            mEditor?.putInt("adsInt", adsInt)?.commit()
        }

        fun get_AdsInt(): Int {
            return preferences?.getInt("adsInt", 3) ?: 0
        }

        //admob interstitial
        fun set_Admob_interstitial_Id(Admob_interstitial_Id: String?) {
            mEditor?.putString("Admob_interstitial_Id", Admob_interstitial_Id)?.commit()
        }

        fun get_Admob_interstitial_Id(): String? {
            return preferences?.getString(
                "Admob_interstitial_Id",
                context?.getString(R.string.interstatatial_ad_id)
            )
        }

        //admob native
        fun set_Admob_native_Id(Admob_native_Id: String?) {
            mEditor?.putString("Admob_native_Id", Admob_native_Id)?.commit()
        }

        fun get_Admob_native_Id(): String? {
            return preferences?.getString(
                "Admob_native_Id",
                context?.getString(R.string.native_ad_id)
            )
        }

        //admob openapp
        fun set_Admob_openapp(Admob_native_Id: String?) {
            mEditor?.putString("Admob_open_Id", Admob_native_Id)?.commit()
        }

        fun get_Admob_openapp(): String? {
            return preferences?.getString(
                "Admob_open_Id",
                context?.getString(R.string.open_ad_id)
            )
        }

        //qureka
        fun set_qureka(yes: Boolean) {
            mEditor?.putBoolean("qureka_Id", yes)?.commit()
        }

        fun get_qureka(): Boolean {
            return preferences?.getBoolean("qureka_Id", false) == true
        }

        fun getData(context: Context) {
            try {
                if (context.isNetworkAvailable()) {
                    if (retrofit == null) {
                        retrofit = Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(
                            OkHttpClient.Builder().addInterceptor { chain: Interceptor.Chain ->
                                chain.proceed(chain.request().newBuilder().build())
                            }.connectTimeout(100, TimeUnit.SECONDS)
                                .readTimeout(100, TimeUnit.SECONDS).build()
                        ).addConverterFactory(GsonConverterFactory.create()).build()
                    }
                    val nativeClient: RetrofitInterface? = retrofit?.create(RetrofitInterface::class.java)
                    nativeClient?.getBtnAd()?.enqueue(object : Callback<Model_Playdata> {
                        override fun onResponse(call: Call<Model_Playdata>, response: Response<Model_Playdata>) {
                            try {
                                response.body()?.let { body ->
                                    if (body.isStatus) {
                                        if (body.data != null) {
                                            checkqureka = body.data.isFlage
                                            if (body.data.isFlage) {
                                                qurekaimage = body.data.image
                                                url = body.data.url
                                                querekatext = body.data.title
                                            }
                                        }
                                    }

                                }
                            } catch (exception: Exception) {
                                exception.printStackTrace()
                            }
                        }

                        override fun onFailure(call: Call<Model_Playdata?>, t: Throwable) {}
                    })
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        fun Context.isNetworkAvailable(): Boolean {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            @SuppressLint("MissingPermission") val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
    }


    override fun onCreate() {
        super.onCreate()

        MobileAds.initialize(this) { }

        context = this
        appOpenManager = AppOpenManager(this)
        preferences = getSharedPreferences("ps", MODE_PRIVATE)
        mEditor = preferences?.edit()
        getData(applicationContext)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        startKoin {
            androidContext(this@MyApplication)
            modules(signletoneModule)
        }
    }

}