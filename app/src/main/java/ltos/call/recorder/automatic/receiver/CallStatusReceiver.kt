package ltos.call.recorder.automatic.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.TelephonyManager
import android.util.Log
import ltos.call.recorder.automatic.services.RecorderOnOffService
import ltos.call.recorder.automatic.services.RecorderService
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.singleton.TempData
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


class CallStatusReceiver : BroadcastReceiver(), KoinComponent {

    private val myPref:MySharedPrefernce by inject()

    override fun onReceive(context: Context, intent: Intent) {

        if(!myPref.getString("isRecordingOn").toBoolean())
        {
            return
        }

        checkSimstate(context)

        when (intent.action) {
            "android.intent.action.PHONE_STATE" -> {
                when (intent.getStringExtra(TelephonyManager.EXTRA_STATE)) {
                    TelephonyManager.EXTRA_STATE_OFFHOOK -> {
                        TempData.outgoingcall = false

                        startServicebyMethod(context)
                    }
                    TelephonyManager.EXTRA_STATE_IDLE -> {
                        val i = Intent(context, RecorderService::class.java)
                        context.stopService(i)
                    }
                    TelephonyManager.EXTRA_STATE_RINGING -> {
                        TempData.callnumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER).toString()
                    }
                }
            }
            "android.intent.action.NEW_OUTGOING_CALL" -> {
                TempData.outgoingcall = true
                TempData.callnumber =
                    intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER).toString()
                startServicebyMethod(context)
            }
            "android.intent.action.BOOT_COMPLETED" -> {
                context.startService(Intent(context, RecorderOnOffService::class.java))
            }
            "android.intent.action.QUICKBOOT_POWERON" -> {
                context.startService(Intent(context, RecorderOnOffService::class.java))
            }
        }
    }

    private fun checkSimstate(context: Context)
    {
        val telMgr = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
        val simState = telMgr?.simState
        when(simState) {
            TelephonyManager.SIM_STATE_ABSENT -> {
                return
            }
            TelephonyManager.SIM_STATE_NETWORK_LOCKED -> {}
            TelephonyManager.SIM_STATE_PIN_REQUIRED -> {}
            TelephonyManager.SIM_STATE_PUK_REQUIRED -> {}
            TelephonyManager.SIM_STATE_READY -> {}
            TelephonyManager.SIM_STATE_UNKNOWN -> {}
        }
    }

    fun startServicebyMethod(context: Context) {

        when (TempData.callnumber.startsWith("+")) {
            true -> {
                TempData.callnumber = TempData.callnumber.substring(3).trim()
            }
        }
        when (myPref.getString("isContact")) {
            "true" -> {
                myPref.getContactList("contactIgnore").forEach {
                    if (it.phoneNumber == TempData.callnumber) {
                        return
                    }
                }
            }
            "false" -> {
                var matched = false
                myPref.getContactList("contactRecord").forEach {
                    if (it.phoneNumber == TempData.callnumber) {
                        matched = true
                    }
                }
                if (!matched) {
                    return
                }
            }
        }
        if (serviceStarted == 1) {
            val i = Intent(context, RecorderService::class.java)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(i)
            } else {
                context.startService(i)
            }
            serviceStarted = 0
        }
    }

    companion object {
        var serviceStarted = 1
    }

}