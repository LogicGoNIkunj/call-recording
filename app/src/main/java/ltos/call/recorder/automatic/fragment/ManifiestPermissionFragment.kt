package ltos.call.recorder.automatic.fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.activity.MainActivity
import ltos.call.recorder.automatic.databinding.FragmentManifiestPermissionBinding
import ltos.call.recorder.automatic.koinmodule.MyApplication
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.singleton.TempData
import org.koin.android.ext.android.inject


class ManifiestPermissionFragment : Fragment() {

    private var binding: FragmentManifiestPermissionBinding? = null
    private val myPref: MySharedPrefernce by inject()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentManifiestPermissionBinding.inflate(inflater, container, false)
        return binding?.root
    }

    private val permission: Unit
        get() {
            var permission = arrayOf(
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.PROCESS_OUTGOING_CALLS,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_CALL_LOG,
            )
            ActivityCompat.requestPermissions(requireActivity(), permission, TempData.PERMISSION_MANIFIEST)
        }

    private fun checkPermission(): Boolean {
        val read = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val write = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val phonecall = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.READ_PHONE_STATE
        )
        val recordaudio =
            ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO)
        val contact =
            ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CONTACTS)
        val calllog =
            ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CALL_LOG)


        val permissiolist = arrayListOf(read, write, phonecall, recordaudio, contact, calllog)

        permissiolist.map { i ->
            if (i != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()

        if (checkPermission()) {
            if (myPref.getString("isFirstTime").toBoolean()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                {
                    activity?.supportFragmentManager
                        ?.beginTransaction()
                        ?.replace(R.id.fragmentContainer, AcessibilityFragment(), null)
                        ?.commit()
                }
                else
                {
                    activity?.supportFragmentManager
                        ?.beginTransaction()
                        ?.replace(R.id.fragmentContainer, BatteryOptimizationFragment(), null)
                        ?.commit()
                }
            } else {

                MyApplication.appOpenManager!!.isAdShow = false
                startActivity(Intent(requireContext(), MainActivity::class.java))
                requireActivity().finish()
            }
        }

        binding?.permissionButton?.setOnClickListener {
            if (!checkPermission()) {
                permission
            } else {
                activity?.supportFragmentManager
                    ?.beginTransaction()
                    ?.replace(R.id.fragmentContainer, AcessibilityFragment(), null)
                    ?.commit()
            }
        }
    }

}