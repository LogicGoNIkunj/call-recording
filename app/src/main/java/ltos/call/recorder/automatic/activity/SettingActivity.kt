package ltos.call.recorder.automatic.activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.databinding.ActivitySettingBinding
import ltos.call.recorder.automatic.databinding.FragmentFeedbackBinding
import ltos.call.recorder.automatic.databinding.FragmentRateusBinding
import ltos.call.recorder.automatic.dataclass.Feedback
import ltos.call.recorder.automatic.services.AllTimeNotificationService
import ltos.call.recorder.automatic.services.RecorderOnOffService
import ltos.call.recorder.automatic.singleton.FileUtility
import ltos.call.recorder.automatic.singleton.MySharedPrefernce
import ltos.call.recorder.automatic.api.RetrofitClient
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SettingActivity : AppCompatActivity() {

    private val myPref: MySharedPrefernce by inject()
    private val fileUtility: FileUtility by inject()
    private var selectedAudioFormat = 1
    private var selectedInboxSize = 4
    private var saveLocation = ""
    private var binding: ActivitySettingBinding? = null
    private var ratebinding: FragmentRateusBinding? = null
    private var rateDialog:AlertDialog? = null

    private var feedbackdialog:AlertDialog? = null
    private var feddbackbinding: FragmentFeedbackBinding? = null
    private var isFatching = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setSettingActivity()
    }

    private fun setSettingActivity() {
        setRecordingSwitch()
        setNotificationSwitch()
        setAllTextView()
        getSelectedAudioFormat()
        getSelectedInboxSize()
        setAllClick()
        createRateDialog()
    }

    private fun setAllTextView() {
        saveLocation = fileUtility.AppFolderName.toString()
        val savefolder = saveLocation.substring(saveLocation.lastIndexOf("0") +1)
        binding?.storageOptionText?.text = savefolder

        try {
            binding?.appversion?.text = "App Version:   " + getPackageManager()
                .getPackageInfo(getPackageName(), 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()

        }
    }

    private fun setAllClick() {
        binding?.audioFormatTitle?.setOnClickListener {
            audioFormatOption()
        }

        binding?.storageTitle?.setOnClickListener {
            saveLocationDialog()
        }

        binding?.inboxTitle?.setOnClickListener {
            inboxSizeOption()
        }

        binding?.recordingTitle?.setOnClickListener {
            startActivity(Intent(this, ContactActivity::class.java))
        }

       binding?.backbtn?.setOnClickListener {
           onBackPressed()
       }

        binding?.shareappbtn?.setOnClickListener {
            val sendIntent = Intent(Intent.ACTION_SEND)
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                "Check out the App at: https://play.google.com/store/apps/details?id=${packageName}"
            )
            sendIntent.type = "text/plain"
            startActivity(Intent.createChooser(sendIntent, "Share Via"))
        }

        binding?.rateappbtn?.setOnClickListener {
            showRateDialog()
        }

        binding?.privacyPolicy?.setOnClickListener {
            if (isOnline()) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy_link))))
            } else {
                Toast.makeText(this, "You are not connected to Internet", Toast.LENGTH_SHORT).show()
            }
        }

        binding?.permission?.setOnClickListener {
            myPref.setString("isFirstTime", "true")
            startActivity(Intent(this, PermissionOneActivity::class.java))
        }
    }

    private fun createRateDialog() {

        if(ratebinding == null)
        {
            ratebinding = FragmentRateusBinding.inflate(layoutInflater)

            ratebinding?.cancelBtn?.setOnClickListener {
                rateDialog?.dismiss()
            }
            rateDialog = AlertDialog.Builder(this)
                .setView(ratebinding?.root)
                .create()
                .apply {
                    window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
                }

            ratebinding?.ratingBar?.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
                if (fromUser) {
                    if (rating == 4f || rating == 5f) {
                        giveRattinf()
                        rateDialog?.dismiss()
                    } else {
                        showfeedbackdialog()
                        rateDialog?.dismiss()
                    }
                }
            }
        }
    }

    private fun showfeedbackdialog() {
        feddbackbinding = FragmentFeedbackBinding.inflate(layoutInflater)
        feedbackdialog =  AlertDialog.Builder(this)
            .setView(feddbackbinding?.root)
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }

        feddbackbinding?.send?.setOnClickListener {
            if(feddbackbinding?.TextTitle?.text.isNullOrEmpty())
            {
                feddbackbinding?.TextTitle?.setError("Please enter a title")
            }
            else if(feddbackbinding?.TextDescription?.text.isNullOrEmpty())
            {
                feddbackbinding?.TextDescription?.setError("Please enter a description ")
            }
            else
            {
                var packageInfo: PackageInfo? = null
                try {
                    packageInfo = getPackageManager()?.getPackageInfo(getPackageName().toString(), 0)
                } catch (e: PackageManager.NameNotFoundException) {
                    e.printStackTrace()
                }
                val appverson = packageInfo!!.versionName
                val appname = resources.getString(R.string.app_name)
                val appversoncode = Build.VERSION.SDK_INT
                val mobilemodel = Build.MODEL
                val packagename: String = getPackageName().toString()
                PostFeedback(
                    appverson,
                    appname,
                    appversoncode.toString(),
                    mobilemodel,
                    packagename,
                    feddbackbinding?.TextTitle?.text.toString(),
                    feddbackbinding?.TextDescription?.text.toString()
                )

            }
        }

        feddbackbinding?.cancel?.setOnClickListener {
            feedbackdialog?.dismiss()
        }

        feedbackdialog?.show()

    }

    private fun PostFeedback(appverson: String?, appname: String, appversoncode: String, mobilemodel: String?, packagename: String, title: String, description: String) {

        if(!isFatching)
        {
            val dialog_ll = ProgressDialog(this)
            dialog_ll.setMessage("Send Feedback...")
            dialog_ll.setCancelable(false)
            dialog_ll.show()
            isFatching = true

            RetrofitClient.getFeedbackClient().sendfeedback(
                appname,
                packagename,
                title,
                description,
                mobilemodel,
                appversoncode,
                appverson
            )?.enqueue(object : Callback<Feedback?> {
                override fun onResponse(call: Call<Feedback?>, response: Response<Feedback?>) {
                    if (response.code() == 200) {
                        assert(response.body() != null)
                        isFatching = false
                        if (response.body()?.status!!) {
                            Toast.makeText(
                                this@SettingActivity,
                                "" + response.body()?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            dialog_ll.dismiss()
                            feddbackbinding?.TextTitle?.text = null
                            feddbackbinding?.TextDescription?.text = null
                            feedbackdialog?.dismiss()
                        }
                    }
                }
                override fun onFailure(call: Call<Feedback?>, t: Throwable) {
                    dialog_ll.dismiss()
                    Toast.makeText(
                        this@SettingActivity,
                        "Please send feedback in playstore",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
    }

    private fun showRateDialog() {
        rateDialog?.show()
        rateDialog?.getWindow()?.setLayout((resources.displayMetrics.widthPixels*0.8).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
    }

    fun giveRattinf() {
//        val rating = ratebinding?.ratingBar?.rating?.toInt()

        try {
            val sb = "market://details?id=" + getPackageName()
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(sb)))
        } catch (unused: ActivityNotFoundException) {
            val sb2 =
                "https://play.google.com/store/apps/details?id=" + getPackageName()
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(sb2)))
        }
         /*else {
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.add(com.tv.casting.screenrecorder.R.id.frag_container, FeedbackFragment())
                ?.addToBackStack(null)
                ?.commit()
        }*/
    }

    private fun saveLocationDialog() {
        AlertDialog.Builder(this)
            .setTitle("Save Location")
            .setMessage(saveLocation)
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }
            .show()

    }

    private fun setRecordingSwitch() {

        when(myPref.getString("isRecordingOn"))
        {
            "true"-> binding?.recordingSwitch?.isChecked = true
            "false"-> binding?.recordingSwitch?.isChecked = false
        }

        binding?.recordingSwitch?.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(cb: CompoundButton?, boolean: Boolean) {
                when(boolean)
                {
                    true -> {startRecordingService()}
                    false -> {stopRecordingService()}
                }
            }
        })
    }

    private fun stopRecordingService() {
        myPref.setString("isRecordingOn", "false")
        startService(Intent(this, RecorderOnOffService::class.java))

        //for notification switch
        myPref.setString("isNotificationOn", "false")
        stopService(Intent(this@SettingActivity, AllTimeNotificationService::class.java))

        binding?.notificationSwitch?.isChecked = false
    }

    private fun startRecordingService() {
        myPref.setString("isRecordingOn", "true")
        startService(Intent(this, RecorderOnOffService::class.java))
    }

    private fun setNotificationSwitch() {
        when(myPref.getString("isNotificationOn"))
        {
            "true"-> binding?.notificationSwitch?.isChecked = true
            "false"-> binding?.notificationSwitch?.isChecked = false
        }
        binding?.notificationSwitch?.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(cb: CompoundButton?, boolean: Boolean) {
                when(boolean)
                {
                    true -> {
                        if(!binding?.recordingSwitch?.isChecked!!)
                        {
                            binding?.notificationSwitch?.isChecked = false
                            Toast.makeText(this@SettingActivity, "Can't turn ON notification while Record Calls is off", Toast.LENGTH_LONG).show()
                            return
                        }
                        myPref.setString("isNotificationOn", "true")
                        startAlltimeNotificationService()
                    }
                    false -> {
                        myPref.setString("isNotificationOn", "false")
                        stopService(Intent(this@SettingActivity, AllTimeNotificationService::class.java))
                    }
                }
            }
        })
    }

    private fun startAlltimeNotificationService()
    {
        val i = Intent(this, AllTimeNotificationService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(i)
        } else {
            startService(i)
        }
    }

    private fun inboxSizeOption()
    {
        val inboxsizedialog = AlertDialog.Builder(this)
            .setTitle("Inbox Size")
            .setSingleChoiceItems(arrayOf("5", "10", "20", "40", "100","200","300","325","500","750","1000"),
                selectedInboxSize,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        when (id) {
                            0 -> myPref.setString("InboxSize", "5")
                            1 -> myPref.setString("InboxSize", "10")
                            2 -> myPref.setString("InboxSize", "20")
                            3 -> myPref.setString("InboxSize", "40")
                            4 -> myPref.setString("InboxSize", "100")
                            5 -> myPref.setString("InboxSize", "200")
                            6 -> myPref.setString("InboxSize", "300")
                            7 -> myPref.setString("InboxSize", "325")
                            8 -> myPref.setString("InboxSize", "500")
                            9 -> myPref.setString("InboxSize", "750")
                            10 -> myPref.setString("InboxSize", "1000")
                            else -> {Toast.makeText(this@SettingActivity, "Something went wrong", Toast.LENGTH_SHORT).show()}
                        }.also { getSelectedInboxSize() }
                        dialog?.dismiss()
                    }
                }
            )
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }
        inboxsizedialog.show()
        inboxsizedialog.getWindow()?.setLayout((resources.displayMetrics.widthPixels*0.7).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
    }

    private fun getSelectedInboxSize() {
        when (myPref.getString("InboxSize")) {
            "5" -> {
                selectedInboxSize = 0
                binding?.inboxOptionText?.text = "5"
            }
            "10" -> {
                selectedInboxSize = 1
                binding?.inboxOptionText?.text = "10"
            }
            "20" -> {
                selectedInboxSize = 2
                binding?.inboxOptionText?.text = "20"
            }
            "40" -> {
                selectedInboxSize = 3
                binding?.inboxOptionText?.text = "40"
            }
            "100" -> {
                selectedInboxSize = 4
                binding?.inboxOptionText?.text = "100"
            }
            "200" -> {
                selectedInboxSize = 5
                binding?.inboxOptionText?.text = "200"
            }
            "300" -> {
                selectedInboxSize = 6
                binding?.inboxOptionText?.text = "300"
            }
            "325" -> {
                selectedInboxSize = 7
                binding?.inboxOptionText?.text = "325"
            }
            "500" -> {
                selectedInboxSize = 8
                binding?.inboxOptionText?.text = "500"
            }
            "750" -> {
                selectedInboxSize = 9
                binding?.inboxOptionText?.text = "750"
            }
            "1000" -> {
                selectedInboxSize = 10
                binding?.inboxOptionText?.text = "1000"
            }
        }
    }

    private fun audioFormatOption() {
        val audioformatdialog = AlertDialog.Builder(this)
            .setTitle("Audio Format")
            .setSingleChoiceItems(arrayOf("AMR", "3GP", "AAC", "MP4"),
                selectedAudioFormat,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        when (id) {
                            0 -> myPref.setString("AudioFormat", "amr")
                            1 -> myPref.setString("AudioFormat", "3gp")
                            2 -> myPref.setString("AudioFormat",  "aac")
                            3 -> myPref.setString("AudioFormat",  "mp4")
                            else -> {Toast.makeText(this@SettingActivity, "Something went wrong", Toast.LENGTH_SHORT).show()}
                        }.also { getSelectedAudioFormat() }
                        dialog?.dismiss()
                    }
                }
            )
            .create()
            .apply {
                window?.setBackgroundDrawable(AppCompatResources.getDrawable(this@SettingActivity,R.drawable.roundcorner_bg))
            }
        audioformatdialog.show()
        audioformatdialog.getWindow()?.setLayout((resources.displayMetrics.widthPixels*0.7).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)


    }

    private fun getSelectedAudioFormat() {
        when (myPref.getString("AudioFormat")) {
            "amr" -> {
                selectedAudioFormat = 0
                binding?.audioFormatOptionText?.text = "AMR"
            }
            "3gp" -> {
                selectedAudioFormat = 1
                binding?.audioFormatOptionText?.text = "3GP"
            }
            "aac" -> {
                selectedAudioFormat = 2
                binding?.audioFormatOptionText?.text = "AAC"
            }
            "mp4" -> {
                selectedAudioFormat = 3
                binding?.audioFormatOptionText?.text = "MP4"
            }
        }
    }

    protected fun isOnline(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        @SuppressLint("MissingPermission") val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }
}