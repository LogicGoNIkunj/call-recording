package ltos.call.recorder.automatic.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ThreeBounce
import ltos.call.recorder.automatic.R
import ltos.call.recorder.automatic.api.RetrofitClient
import ltos.call.recorder.automatic.dataclass.Model_Ads
import ltos.call.recorder.automatic.koinmodule.MyApplication
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Boolean

class SplashActivity : AppCompatActivity() {


    companion object {
        var publisher_yes = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpAnimation()
        getAdsData()
        Handler().postDelayed({ MyApplication.appOpenManager?.showAdIfAvailable(true) },
            5000)
    }

    private fun getAdsData() {
        RetrofitClient.getAdClient().getAdId(34)!!.enqueue(object : Callback<Model_Ads?> {
            override fun onResponse(call: Call<Model_Ads?>, response: Response<Model_Ads?>) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body()!!.isStatus()) {
                            if (response.body()!!.getData() != null) {
                                MyApplication.set_AdsInt(response.body()!!.getData().getPublisher_id())
                                if (response.body()!!.getData().getPublishers() != null) {
                                    publisher_yes = true
                                    if (MyApplication.get_AdsInt() == 1) {
                                        if (response.body()!!.getData().getPublishers().getAdmob() != null) {
                                            if (response.body()!!.getData().getPublishers().getAdmob().getAdmob_interstitial() != null) {
                                                MyApplication.set_Admob_interstitial_Id(response.body()!!.getData().getPublishers().getAdmob().getAdmob_interstitial().getAdmob_interstitial_id())
                                            } else{
                                                MyApplication.set_Admob_interstitial_Id(resources.getString(
                                                        R.string.interstatatial_ad_id
                                                    ))
                                            }
                                            if (response.body()!!.getData().getPublishers().getAdmob().getAdmob_native() != null) {
                                                MyApplication.set_Admob_native_Id(response.body()!!.getData().getPublishers().getAdmob().getAdmob_native().getAdmob_Native_id())
                                            } else {
                                                MyApplication.set_Admob_native_Id(resources.getString(
                                                        R.string.native_ad_id
                                                    ))
                                            }
                                            if (response.body()!!.getData().getPublishers().getAdmob().getAdmob_open() != null) {
                                                MyApplication.set_Admob_openapp(response.body()!!.getData().getPublishers().getAdmob().getAdmob_open().getAdmob_Open_id())
                                            } else {
                                                MyApplication.set_Admob_openapp(resources.getString(
                                                        R.string.open_ad_id
                                                    ))
                                            }
                                            if (response.body()!!.data.publishers.admob.qureka_open != null) {
                                                MyApplication.set_qureka(Boolean.parseBoolean(response.body()!!.data.publishers.admob.qureka_open.querka_id))
                                            } else {
                                                MyApplication.set_qureka(false)
                                            }
                                        } else {
                                            MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                            MyApplication.set_Admob_interstitial_Id(resources.getString(
                                                    R.string.interstatatial_ad_id
                                                ))
                                            MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                                            MyApplication.set_qureka(false)
                                        }
                                    } else {
                                        MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                        MyApplication.set_Admob_interstitial_Id(resources.getString(
                                                R.string.interstatatial_ad_id
                                            ))
                                        MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                                        MyApplication.set_qureka(false)
                                    }
                                } else {
                                    MyApplication.set_Admob_native_Id("1234")
                                    MyApplication.set_Admob_interstitial_Id("1234")
                                    MyApplication.set_Admob_openapp("1234")
                                    MyApplication.set_qureka(false)
                                }
                            } else {
                                MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                                MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                                MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                                MyApplication.set_qureka(false)
                            }
                        } else {
                            MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                            MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                            MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                            MyApplication.set_qureka(false)
                        }
                    } else {
                        MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                        MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                        MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                        MyApplication.set_qureka(false)
                    }
                } else {
                    MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                    MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                    MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                    MyApplication.set_qureka(false)
                }
            }

            override fun onFailure(call: Call<Model_Ads?>, t: Throwable) {
                MyApplication.set_Admob_native_Id(resources.getString(R.string.native_ad_id))
                MyApplication.set_Admob_interstitial_Id(resources.getString(R.string.interstatatial_ad_id))
                MyApplication.set_Admob_openapp(resources.getString(R.string.open_ad_id))
                MyApplication.set_qureka(false)
            }
        })
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setUpAnimation() {
        val relativeLayout = RelativeLayout(this)
        val progressBar = ProgressBar(this, null, 16842874)
        val doubleBounce: Sprite = ThreeBounce()
        doubleBounce.setColor(resources.getColor(R.color.black))
        progressBar.indeterminateDrawable = doubleBounce
        val layoutParams = RelativeLayout.LayoutParams(110, 110)
        layoutParams.addRule(14)
        layoutParams.addRule(12)
        layoutParams.setMargins(0, 0, 0, 50)
        relativeLayout.addView(progressBar, layoutParams)
        setContentView(relativeLayout)
    }


    override fun onBackPressed() {

    }


}